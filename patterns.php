<?php
// Mirror Triangle
$s = 5;
for ($i=0; $i < $s; $i++) { 
	for ($j=0; $j < 2*$s-1; $j++) { 
		if($j > $s-$i-2){
			if($j < $s+$i){
				echo "*";
			}
		}else{
			echo "&nbsp;&nbsp;";
		}

	}
	echo "<br />";
}

for ($i=$s; $i > 0; $i--) { 
	for ($j=0; $j < 2*$s-1; $j++) { 
	
		if($j > $s-$i){
			if($j < $s+$i-2){
				echo "*";
			}
		}else{
			echo "&nbsp;&nbsp;";
		}
	}
	echo "<br />";
}

// Pattern 2 
// N
// Ni
// Nit
// Niti
// Nitis
// Nitish
$s = 'Nitish';
$len = strlen($s);
for ($i=0; $i < $len; $i++) { 
	for ($j=0; $j <= $i; $j++) { 
		# code...
		echo $s[$j];
	}
	echo "<br />";
}

// Pattern 3
// Nitish
// Nitis
// Niti
// Nit
// Ni
// N
$s = 'Nitish';
$len = strlen($s);
for ($i=$len; $i > 0; $i--) { 
	for ($j=0; $j < $i; $j++) { 
		# code...
		echo $s[$j];
	}
	echo "<br />";
}
